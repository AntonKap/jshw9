
function createList(array, parent = document.body) {
    let newUl = document.createElement('ul')
    parent.append(newUl);
    array.forEach(function(item, index) {
        if (typeof item === 'object'){
            createList(item, newUl)
        }else{
      let li = document.createElement('li');
      li.textContent = `${item}`;
      newUl.append(li);
      }
    });
   
    }

    let timers = document.createElement('div');
    document.body.prepend(timers)
    let seconds = 5;

function clearPage() {
    document.body.innerHTML = ''; 
}
function updateTimer() {
    timers.textContent = `Time to clear page ${seconds}`;
    seconds--;

    if (seconds < 0) {
        clearInterval(timerInterval);
        clearPage();
    }
}

updateTimer(); 

const timerInterval = setInterval(updateTimer, 1000); 



  let array1 = ["hello", "world", ["Kiev", "Kharkiv"], "Odessa", "Lviv"];
  createList(array1)
  let array2 = ["1", "2", "3", "sea", "user", 23];
  createList(array2)